function ColorMe(color)
	-- Vimspectr
--    color = color or 'vimspectr180-dark'
--	vim.g.vimspectrItalicComment = 'on'
--	vim.cmd('colorscheme vimspectr150-dark')

    -- Main
--    vim.g.fluoromachine_airline = 1
--    vim.g.airline_theme = 'fluoromachine'
    vim.cmd('colorscheme fluoromachine')

	-- Custom Colors
    -- - Shades of Purple
    --vim.cmd('hi LineNr guifg=#37dd21 guibg=#28284e')
    --vim.api.nvim_set_hl(0, 'Comment', { italic=true })
    --vim.cmd('hi Comment guifg=#b362ff guibg=NONE')
end

ColorMe()
