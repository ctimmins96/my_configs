local Bind = {}

function Bind.nmap(tbl)
    vim.keymap.set("n", tbl[1], tbl[2], tbl[3])
end

function Bind.imap(tbl)
    vim.keymap.set('i', tbl[1], tbl[2], tbl[3])
end

function Bind.xmap(tbl)
    vim.keymap.set('x', tbl[1], tbl[2], tbl[3])
end

function Bind.ismap(tbl)
    vim.keymap.set({"i", "s"}, tbl[1], tbl[2], tbl[3])
end

return Bind
